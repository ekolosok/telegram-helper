# telegram-helper

## Name
Simple Telegram Democratic Bot

## Description
This is a very simple bot written very quickly to serve a single purpose: fairly and randlomly select 3 users from a telegram chat (meant to be launched on a weekly basis), who will later be responsible for administrating the chat in the following week. 

## Prerequisites and Usage
0. Install the required packages: 
    `pip install -r requirements.txt`

1. Obtain the necessary authentication keys and put them into a `config.yaml` file. 
The keys should contain the following entries: 
* api_id: 1234567 
* api_hash: examplehash123
* bot_token: digits:LettersAndNumbers123
* bot_name: bot_username_in_tg

The `api_id` and `api_hash` keys can be obtained through the bot @BotFather as described [here](https://core.telegram.org/bots/api) (instantly); the `bot_token` and `bot_name` by filling out a form [here](https://my.telegram.org/apps) (instantly, prerequisite is a valid phone number).  

2. Launch the bot server on your computer with `python python-bot.py`. A message 'Running...' will be displayed. 

3. The bot is now ready to listen to commands. You can check it by opening a chat with its username (`bot_name`) and typing `/hello`. Note: `/hello` is a healthcheck command. The bot should reply with hello + username.

4. Add the bot to the group chat, with admin rights.

5. `/getrandomusers` will scan the list of all members that have not left the chat and select 3 of them. The bot should return a list of usernames, with each username on a new line.

## Support
Create an issue here. 

## Roadmap
Future ideas: remove all chat bots from consideration; automatically promote to admins when the selected users have accepted. 

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
done by Ekaterina, discussed with Alexander and Ilya  

## License
MIT

## Project status
This script was meant to be short, transparent and ready ASAP. I doubt I will ever develop this into a real project. 