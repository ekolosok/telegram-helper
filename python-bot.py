#!/usr/bin/env python
# coding: utf-8

import random
import yaml

from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes
from telethon import TelegramClient

# Read config with auth parameters
with open('config.yaml') as f:
    cfg = yaml.load(f, Loader=yaml.FullLoader)

api_id = cfg['api_id']
api_hash = cfg['api_hash']
bot_token = cfg['bot_token']
bot_name = cfg['bot_name']

# Start the bot
bot = TelegramClient('bot', api_id, api_hash).start(bot_token=bot_token)

# Getting info about current chat users
async def get_users(client, group_id):
    users = []
    async for user in client.iter_participants(group_id):
        #select only active users and don't select this bot
        if not user.deleted and user.username != bot_name: 
            users.append({'id': user.id, 'username': user.username})
            print('id:', user.id, 'username:', user.username) 
    return users

# Healthcheck function for the bot
async def hello(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(f'Hello {update.effective_user.first_name}')

# Main function that selects the 3 users and prints them to the chat
async def getrandomusers(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    chat_id=update.effective_chat.id
    members_in_chat = await get_users(bot, chat_id)

    # Filter out the users who only have an id but not a username;
    # add the tag marker @ to the username to call the person if they are selected
    usernames = ['@' + x['username'] for x in members_in_chat if x['username']]

    # Select n random users:
    n = 3
    if n < len(usernames):
        selected_usernames = random.sample(usernames, 3)
    else:
        selected_usernames = usernames

    await context.bot.send_message(
        chat_id=chat_id,
        text='\n'.join(selected_usernames)
    )

app = ApplicationBuilder().token(bot_token).build()

app.add_handler(CommandHandler('hello', hello))
app.add_handler(CommandHandler('getrandomusers', getrandomusers))

print('Running...')
app.run_polling()

